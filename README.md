![Vizury Logo](https://bitbucket.org/vizury/vizury-android-sdk/raw/7948901a6fea9c630f09023019246a8da0201027/resources/VizuryLogo.png)

## Summary
 This is Android SDK integration guide.
 
## Components
  * [Example app](#markdown-header-example-app)
  * [Basic Integration](#markdown-header-basic-integration)
	* [Getting the SDK](#markdown-header-getting-the-sdk)
	* [Add Google Play Services](#markdown-header-add-google-play-services)
	* [Manifest File Changes](#markdown-header-manifest-file-changes)
	* [Vizury SDK Initialization](#markdown-header-vizury-sdk-initialization)
	* [Event Logging](#markdown-header-event-logging)
  * [Push Notifications](#markdown-header-push-notifications)
  	* [Enabling FCM](#markdown-header-enabling-fcm)
  	* [Configuring Application](#markdown-header-configuring-application)
  * [Additional configurations](#markdown-header-additional-configurations)
  	* [Offline caching](#markdown-header-offline-caching)
  	* [Controlling fcm registration and message handling](#markdown-header-controlling-fcm-registration-and-message-handling)
    * [Callback for notification received](#markdown-header-callback-for-notification-received)
  * [Hybrid App Integration](#markdown-header-hybrid-app-integration)
  * [React Native Integration](#markdown-header-react-native-integration)

## Example app

Examples on how the Vizury SDK can be integrated.
`examples/HelloVizury` is a sample app having a basic integration with vizury SDK.


## Basic Integration

### Getting the SDK
Vizury Android Library is available as a maven dependency. Add jcenter in repositories, in your top level build.gradle file
```
repositories {
    jcenter()
}
```
Add the following dependency in your build.gradle file under app module

```
implementation 'com.vizury.mobile:VizurySDK:6.2.4'
//If android X migration is done
implementation 'com.vizury.mobile:VizurySDK:6.2.4-androidx'
//needed for VizurySDK
implementation 'joda-time:joda-time:2.9.4'
implementation 'com.google.code.gson:gson:2.8.5'
```

### Add Google Play Services

Since the 1st of August of 2014, apps in the Google Play Store must use the [Google Advertising ID][google_ad_id] to 
uniquely identify devices. To allow the vizury SDK to use the Google Advertising ID, you must integrate the 
[Google Play Services][google_play_services]. If you haven't done this yet, follow these steps:

Open the build.gradle file of your app and find the dependencies block. Add the following line:
```
implementation 'com.google.android.gms:play-services-base:15.0.0'
implementation 'com.google.android.gms:play-services-ads:15.0.0'
```

### Manifest File Changes

In the Package Explorer open the AndroidManifest.xml of your Android project and make the following changes


Add the below meta tags, replacing the place holders with associated values.

```xml
<meta-data
	android:name="Vizury.VRM_ID"
	android:value="{PACKAGE_ID}" />
<meta-data
	android:name="Vizury.SERVER_URL"
	android:value="{SERVER_URL}" />
<meta-data
	android:name="Vizury.DEBUG_MODE"
	android:value="{true/false}" /> 
```

`PACKAGE_ID` and `SERVER_URL` will be provided by the vizury account manager.
`DEBUG_MODE` is used to show vizury debug/error/info logs. Set this to false when going to production.

### Vizury SDK Initialization

In your MainActivity.java inside onCreate(Bundle SavedInstance) add following code snippet

```java
import com.vizury.mobile.VizuryHelper;
@Override
protected void onCreate(Bundle savedInstanceState) {
	//your code		    
	VizuryHelper.getInstance(getApplicationContext()).init();
	//your code
}
```

### Event Logging

When a user browse through the app, various activities happen e.g. visiting a product, adding the product to cart, making purchase, etc. These are called events. Corresponding to each event, app needs to pass certain variables to the SDK which the SDK will automatically pass to Vizury servers.

Add the following code in each view where the event is to be tracked - 

```java
 VizuryHelper.getInstance(context).logEvent(eventname, attributes);
```
```
Where 
 eventname   : name of the event
 attributes  : AttributeBuilder object containing the attributes. You can set multiple attributes.
```

Eg.
```java
import com.vizury.mobile.VizuryHelper;

AttributeBuilder builder = new AttributeBuilder.Builder()
	.addAttribute("pid", "AFGEMSBBLL")
	.addAttribute("quantity", "1")
	.addAttribute("price", "876")
	.addAttribute("category","clothing")
	.build();

VizuryHelper.getInstance(context).logEvent("product page", builder);
```

You can also pass a JSONObject or a JSONArray as an attribute. 
For passing a JSONObject or JSONArray you can do something like this :

```java
JSONObject attrs;           // your attributes are in a JSONObject.
AttributeBuilder builder = new AttributeBuilder.Builder()
	.addAttribute("productAttributes", attrs.toString())
	.build(); 
```

## Push Notifications

Vizury sends push notifications to Android devices using [Firebase Cloud Messaging][FCM].

## Enabling FCM

Create a FCM project on [fcm-console] if you dont already have one. After creating your project, click the `Add App` button and select the option to add Firebase to your app. After this enter your app's package id. Follow the subsequent steps to download a `google-services.json` file. After this click on the `Cloud Messaging` Tab and note down the server key. This key has to be entered in the vizury dashboard.

## Configuring Application

### Add Firebase SDK

a) Place the `google-services.json` that you had downloaded from the FCM console in the app module.

![add_google_services](https://bitbucket.org/vizury/vizury-android-sdk/raw/7b4bae54d36ebbca7b0b08856086354915228f90/resources/add_google_services_json.png)


b) Gradle changes

Project-level build.gradle (`<project>/build.gradle`):

```
buildscript {
  dependencies {
    // Add this line
    classpath 'com.google.gms:google-services:4.2.0'
  }
}
```
App-level build.gradle (`<project>/<app-module>/build.gradle`):

```
dependencies {
  // Add this line
  implementation 'com.google.firebase:firebase-messaging:17.3.4'
}
...
// Add to the bottom of the file
apply plugin: 'com.google.gms.google-services'
```


### Manifest changes

In the Package Explorer open the AndroidManifest.xml of your Android project and and make the following changes

Add the following meta-tags
```xml
<meta-data
	android:name="Vizury.NOTIFICATION_ICON"
	android:resource="{NOTIFICATION_ICON}" />
<meta-data
	android:name="Vizury.NOTIFICATION_ICON_SMALL"
	android:resource="{NOTIFICATION_ICON_SMALL}" />
```

`NOTIFICATION_ICON` is the icon that comes at the left of a notification

`NOTIFICATION_ICON_SMALL` is the icon that comes at the notification bar. This should be white icon on a transparent background

Refer [Android Notifications][android_notifications] and [Style Icons][style_icons] for more details.

Add the following services for receiving the fcm token and messages.

```xml
<service
	android:name="com.vizury.mobile.Push.VizFirebaseMessagingService">
        <intent-filter>
		<action android:name="com.google.firebase.MESSAGING_EVENT"/>
	</intent-filter>
</service>
```

`Mandatory intent service for doing the heavy lifting`
```xml
<service 
	android:name="com.vizury.mobile.Push.VizIntentService"
	android:exported="false">
</service>
```

## Additional configurations

### Offline caching

If your app supports offline features and you want to send the user behaviour data to vizury while he is offline, add the following tag in the manifest.

```xml
<meta-data
	android:name="Vizury.DATA_CACHING"
	android:value="true"/>
```
  	

### Controlling fcm registration and message handling

If you want to control FCM registration or FCM message handling then make these changes. In the Package Explorer open the AndroidManifest.xml of your Android project.

a) Remove the `VizFirebaseMessagingService` from the manifest if added.

b) Make the following code changes

`Pass the FCM token to vizury. Make sure that you call this everytime FCM Token is refreshed` 
`On receving any push message call the vizury api for checking and parsing the payload`

```java
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        VizLog.info("VizFirebaseMessagingService. onNewToken. Token is " + s);
        // pass the refreshed token to vizury
        VizuryHelper.getInstance(getApplicationContext()).setGCMToken(s);
    }
	
	@Override
    public void onMessageReceived(RemoteMessage message){
	    Map data = message.getData();
        if (Utils.getInstance(getApplicationContext()).isPushFromVizury(data))
			PushHandler.getInstance(getApplicationContext()).handleNotificationReceived(data);
        else {
            // your own logic goes here
        }
    }
}
```

### Callback for notification received

You can create a service and add the below meta tag in the manifest. The declared service will get a callback whenever a notification is received

```xml
<meta-data
    android:name="Vizury.APP_GCM_SERVICE"
    android:value="{GCM_SERVICE_CLASS_NAME}" />
```	

## Hybrid App Integration

The hybrid app is written with the same technology used for websites and mobile web implementations and is hosted or run on a local container on a mobile device.
Hybrid applications use web view control to display HTML and JavaScript files in a full-screen format using the native browser rendering engine (not the browser itself).
This means that the HTML and JavaScript used to build the hybrid app are provided/processed by the WebKit rendering engine and displayed to the user in full-screen web view control, not in the browser.

### a) Basic Integration 

* [Getting the SDK](#markdown-header-getting-the-sdk)
* [Add Google Play Services](#markdown-header-add-google-play-services)
* [Manifest File Changes](#markdown-header-manifest-file-changes)
* [Vizury SDK Initialization](#markdown-header-vizury-sdk-initialization)

### Code changes in Android side: 

`Include the below code inside the onCreate method of the Class to which intent is fired  ` 

Reference Java file : [WebViewActivity]

```java
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		CookieManager.getInstance().acceptThirdPartyCookies(webView);
		CookieManager.getInstance().setAcceptThirdPartyCookies(webView,true);

		String cookieUrl = Utils.getInstance(getApplicationContext()).getCookieUrl();
		// eg for , your website Domain:  vizury.com
		//this is  stamped in the cookie , so that the cookie url can taken from js side and used to send data to vizury servers
		CookieManager.getInstance().setCookie("your website Domain","cookieUrl="+cookieUrl);
}

```


### Code changes in Html / js side

`Include the below code On load of Webview page inside the HTML file ,  inside the <script> tag`
 
```java
 function fireVizardPing(eventName,JSonObject){
    var cookieValue = ";" + document.cookie;
    var cookieValues = cookieValue.split(";");
    for(var i=0;i<cookieValues.length;i++){
      var checkValue = cookieValues[i];
      if(checkValue.includes("analyze")){
        var analyzeUrl= checkValue.replace("cookieUrl=","");
        analyzeUrl = analyzeUrl.replace("{eventName}",eventName);
        url = analyzeUrl +  "&json1="+JSonObject.toString();
        document.write(url);
        fetch(url);
		break;
      }
    }
}
```
When a user browse through the app, various activities happen e.g. visiting a product, adding the product to cart, making purchase, etc. These are called events. Corresponding to each event, app needs to pass certain variables to the SDK which the SDK will automatically pass to Vizury servers.

Add the following code in each view where the event is to be tracked - 

```java
	//eventName - eventToBeTracked
	//JSonObject - Extra Details to be sent to Vizury eg  { price : 870 } 
	fireVizardPing(eventName,JSonObject);
```
# React Native Integration

React Native is a mobile app development framework that enables the development of multi-platform Android and iOS apps using native UI elements. 
It is based on the JavaScriptCore runtime and Babel transformers. With this setup RN supports new JavaScript (ES6+) features, e.g. arrow functions, async/await etc.

## Follow the integration steps

### a) Basic Integration 

* In the case your native application doesn't use any native modules already or you don't find any android folder inside you root directory of your project
   Follow this step [react-native-android] 
* [Getting the SDK](#markdown-header-getting-the-sdk)
* [Add Google Play Services](#markdown-header-add-google-play-services)
* [Manifest File Changes](#markdown-header-manifest-file-changes)
* [Push Notification ](#markdown-header-push-notifications)
 

### b) Make the following code changes (Java Side)

Create a Class : VizuryManagerModule inside the directory /android/app/src/main/java/com/

Paste the following code inside the VizuryManagerModule.java

Code reference [Vizury-Manager-Module]

```java
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.vizury.mobile.AttributeBuilder;
import com.vizury.mobile.VizuryHelper;

import java.util.HashMap;
import java.util.Map;

public class VizuryManagerModule extends ReactContextBaseJavaModule {
    
	public static final String TAG      = "Vizury";

    public VizuryManagerModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        VizuryHelper.getInstance(getReactApplicationContext()).init();
    }

    @Override
    public String getName() {
        return "VizuryManager";
    }

    @ReactMethod
    public void addEvent(ReadableMap hashMapData, String keyPage) {
        HashMap<String,String> attributes=(HashMap) hashMapData.getMap("attributes").toHashMap();
        Log.d(TAG,"Attributes Map :  " + attributes.toString());
        AttributeBuilder builder = new AttributeBuilder.Builder().build();
        HashMap<String,String> data= builder.getAttributeMap();
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            data.put(entry.getKey(),entry.getValue());
        }
        VizuryHelper.getInstance(getReactApplicationContext()).logEvent(keyPage, builder);
    }
}

```

Create a Class : VizuryManagerPackage inside the directory /android/app/src/main/java/com/

Paste the following code inside the VizuryManagerPackage.java

Code reference [Vizury-Package-Module]

```java
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VizuryManagerPackage implements ReactPackage {

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        modules.add(new VizuryManagerModule(reactContext));
        return modules;
    }
}

```

Make the following code changes inside the MainApplication.java 
Code Reference : [Sample-Main-Applicaion]
   
`Inside the java method getPackages`
```java 
	@Override
	  protected List<ReactPackage> getPackages() {
		  return Arrays.<ReactPackage>asList(
				  new MainReactPackage(),
				  //Vizury package added to get reflected and to be used from React side 
				  new VizuryManagerPackage()
		  );
	  }
```
### c) Make the following code changes (React Side)

Inside the *.js file 

1) Import the native module

```
import { NativeModules} from 'react-native';
const { VizuryManager } = NativeModules;

```
2) Sample function to send events

code reference : [App-JS]
   
When a user browse through the app, various activities happen e.g. visiting a product, adding the product to cart, making purchase, etc. These are called events. Corresponding to each event, app needs to pass certain variables to the SDK which the SDK will automatically pass to Vizury servers.

Add the following code in each view where the event is to be tracked 

```
	  var attributesMap = {};
	  attributesMap["pid"] = "AFGEMSBBLL";
	  attributesMap["quantity"] = "1";
	  attributesMap["price"] = "876";
	  attributesMap["category"] = "clothing";
	  var sendingMap = {};
	  sendingMap["attributes"] = attributesMap;
	  VizuryManager.addEvent(sendingMap, 'product page');

```
[WebViewActivity]: https://bitbucket.org/vizury/vizury-android-sdk/src/master/examples/HelloVizury/app/src/main/java/com/vizury/hellovizury/WebViewActivity.java  
[APP-JS]: https://bitbucket.org/vizury/vizury-android-sdk/src/vizuryreactnativeexample/examples/VizuryReactNative/App.js
[Sample-Main-Applicaion]: https://bitbucket.org/vizury/vizury-android-sdk/src/vizuryreactnativeexample/examples/VizuryReactNative/android/app/src/main/java/com/vizuryreactnative/MainApplication.java 
[Vizury-Package-Module]: https://bitbucket.org/vizury/vizury-android-sdk/src/vizuryreactnativeexample/examples/VizuryReactNative/android/app/src/main/java/com/vizuryreactnative/VizuryManagerPackage.java  
[Vizury-Manager-Module]: https://bitbucket.org/vizury/vizury-android-sdk/src/vizuryreactnativeexample/examples/VizuryReactNative/android/app/src/main/java/com/vizuryreactnative/VizuryManagerModule.java
[react-native-android]: https://docs.expo.io/versions/latest/expokit/eject/
[google_ad_id]:                 https://support.google.com/googleplay/android-developer/answer/6048248?hl=en
[google_play_services]:         http://developer.android.com/google/play-services/setup.html
[google_developer_console]:	https://developers.google.com/mobile/add?platform=android
[fcm-console]:			https://console.firebase.google.com/
[android_notifications]:	https://material.google.com/patterns/notifications.html
[style_icons]:			https://material.google.com/style/icons.html
[FCM]:				https://firebase.google.com/docs/cloud-messaging/
[firebase-link]:		https://firebase.google.com/docs/cloud-messaging/android/client
[FCMTokenReader]:		https://github.com/vizury/vizury-android-sdk/blob/master/examples/HelloVizury-FCM/app/src/main/java/com/vizury/hellovizuryfcm/FCMTokenReader.java
