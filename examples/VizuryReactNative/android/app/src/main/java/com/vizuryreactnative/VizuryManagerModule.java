package com.vizuryreactnative;

import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.vizury.mobile.AttributeBuilder;
import com.vizury.mobile.VizuryHelper;

import java.util.HashMap;
import java.util.Map;

public class VizuryManagerModule extends ReactContextBaseJavaModule {
    public static final String TAG      = "Vizury";

    public VizuryManagerModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        checkPlayServices();
        VizuryHelper.getInstance(getReactApplicationContext()).init();
    }

    @Override
    public String getName() {
        return "VizuryManager";
    }

    @ReactMethod
    public void addEvent(ReadableMap hashMapData, String keyPage) {
        HashMap<String,String> attributes=(HashMap) hashMapData.getMap("attributes").toHashMap();
        Log.i(TAG,"Attributes Map :  " + attributes.toString());
        AttributeBuilder builder = new AttributeBuilder.Builder().build();
        HashMap<String,String> data= builder.getAttributeMap();
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            data.put(entry.getKey(),entry.getValue());
        }
        VizuryHelper.getInstance(getReactApplicationContext()).logEvent(keyPage, builder);
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getReactApplicationContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getCurrentActivity(), resultCode, 2404).show();
                Log.i(TAG,"Google play services error. Is user resolvable with resultcode " + resultCode);
            } else {
                Log.e(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }
}
