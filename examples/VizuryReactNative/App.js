import React from 'react';
import { Text, View, StyleSheet ,NativeModules,Button} from 'react-native';
//Importing Vizury modules
const { VizuryManager } = NativeModules;
const styles = StyleSheet.create({
  center: {
    alignItems: 'center'
  }
})

function Greeting(props) {
  return (
    <View style={styles.center}>
      <Text>Hello {props.name}!</Text>
    </View>
  );
}

function LotsOfGreetings() {
  return (
    <View style={[styles.center, {top: 50}]}>
      <Greeting name='Vizury' />
      <Button title={'Send Event'} onPress={sendEventsToVizury} />
    </View>
  );
}

function sendEventsToVizury(){
/*
  AttributeBuilder builder = new AttributeBuilder.Builder()
    .addAttribute("pid", "AFGEMSBBLL")
    .addAttribute("quantity", "1")
    .addAttribute("price", "876")
    .addAttribute("category","clothing")
    .build();

VizuryHelper.getInstance(context).logEvent("product page", builder);
*/
  var attributesMap = {};
  attributesMap["pid"] = "AFGEMSBBLL";
  attributesMap["quantity"] = "1";
  attributesMap["price"] = "876";
  attributesMap["category"] = "clothing";
  var sendingMap = {};
  sendingMap["attributes"] = attributesMap;
  var akil = VizuryManager.addEvent(sendingMap, 'product page');
  // Console.log(akil);
}

export default LotsOfGreetings;



